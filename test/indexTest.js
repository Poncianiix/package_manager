const sumar = require('../index');
const assert = require('assert');
const { deepStrictEqual } = require('assert');

//Assert = Afirmacion 
// 50 % test


describe("Probar la suma de los numero",()=>{
    //Afirmamos que 5 + 5 = 10

    
    it("5 + 5 = 10",()=>{
        assert.equal(10, sumar(5,5));
    });
    
    //Afirmacion que 5 + 7 != 10

    it("5 + 7 != 10",()=>{
        assert.notEqual(10,sumar(5,7));
    });
})